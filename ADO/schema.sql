-- Création de la table "Personne"
CREATE TABLE IF NOT EXISTS Personne (
    PersonneId INTEGER PRIMARY KEY AUTOINCREMENT,
    Nom TEXT NOT NULL,
    Prenom TEXT NOT NULL,
    DateNaissance DATE NOT NULL
);

-- Création de la table "Adresse"
CREATE TABLE IF NOT EXISTS Adresse (
    AdresseId INTEGER PRIMARY KEY AUTOINCREMENT,
    PersonneId INTEGER NOT NULL,
    Rue TEXT NOT NULL,
    Ville TEXT NOT NULL,
    CodePostal TEXT NOT NULL,
    FOREIGN KEY (PersonneId) REFERENCES Personne(PersonneId) ON DELETE CASCADE ON UPDATE CASCADE
);
