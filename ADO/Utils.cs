using System.Data;

class Utils
{
    public static IDbDataParameter CreateParameter(IDbDataParameter parameter, string parameterName, object value, DbType type)
    {
        parameter.ParameterName = parameterName;
        parameter.Value = value;
        parameter.DbType = type;

        return parameter;
    }
}