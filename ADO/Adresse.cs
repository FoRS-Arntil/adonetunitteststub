namespace ADO;

public class Adresse
{
    public int AdresseId { get; set; }
    public int PersonneId { get; set; }
    public string Rue { get; set; }
    public string Ville { get; set; }
    public string CodePostal { get; set; }
}
