using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;

namespace ADO;

public class AdresseRepository
{
    private readonly string connectionString;
    private readonly DbProviderFactory factory;

    public AdresseRepository(DbProviderFactory factory, string connectionString)
    {
        this.factory = factory;
        this.connectionString = connectionString;
    }

    // Méthode pour récupérer une adresse par son ID
    public Adresse ObtenirAdresseParId(int adresseId)
    {
        using (IDbConnection connection = new SQLiteConnection(connectionString))
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Adresse WHERE AdresseId = @AdresseId";
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@AdresseId", adresseId, DbType.Int32));

                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return MapAdresse(reader);
                    }
                }
            }
        }

        return null; // Aucune adresse trouvée pour l'ID spécifié
    }

    // Méthode pour créer une nouvelle adresse
    public void AjouterAdresse(Adresse adresse)
    {
        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();
            
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO Adresse (PersonneId, Rue, Ville, CodePostal) VALUES (@PersonneId, @Rue, @Ville, @CodePostal); SELECT last_insert_rowid();";
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@PersonneId", adresse.PersonneId, DbType.Int32));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Rue", adresse.Rue, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Ville", adresse.Ville, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@CodePostal", adresse.CodePostal, DbType.String));

                adresse.AdresseId = Convert.ToInt32(command.ExecuteScalar());
            }
        }
    }

    // Méthode pour récupérer toutes les adresses
    public List<Adresse> ObtenirToutesLesAdresses()
    {
        List<Adresse> adresses = new List<Adresse>();

        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Adresse";
                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Adresse adresse = MapAdresse(reader);
                        adresses.Add(adresse);
                    }
                }
            }
        }

        return adresses;
    }

    // Méthode pour mettre à jour une adresse
    public void MettreAJourAdresse(Adresse adresse)
    {
        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE Adresse SET PersonneId = @PersonneId, Rue = @Rue, Ville = @Ville, CodePostal = @CodePostal WHERE AdresseId = @AdresseId";
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@PersonneId", adresse.PersonneId, DbType.Int32));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Rue", adresse.Rue, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Ville", adresse.Ville, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@CodePostal", adresse.CodePostal, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@AdresseId", adresse.AdresseId, DbType.Int32));

                command.ExecuteNonQuery();
            }
        }
    }

    // Méthode pour supprimer une adresse
    public void SupprimerAdresse(int adresseId)
    {
        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@AdresseId", adresseId, DbType.Int32));

                command.ExecuteNonQuery();
            }
        }
    }

    // Méthode utilitaire pour mapper les données du lecteur à l'objet Adresse
    public static Adresse MapAdresse(IDataReader reader)
    {
        return new Adresse
        {
            AdresseId = Convert.ToInt32(reader["AdresseId"]),
            PersonneId = Convert.ToInt32(reader["PersonneId"]),
            Rue = (string) reader["Rue"],
            Ville = (string) reader["Ville"],
            CodePostal = (string) reader["CodePostal"]
        };
    }
}
