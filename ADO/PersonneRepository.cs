using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;

namespace ADO;

public class PersonneRepository
{
    private readonly DbProviderFactory factory;
    private readonly string connectionString;

    public PersonneRepository(DbProviderFactory factory, string connectionString)
    {
        this.factory = factory;
        this.connectionString = connectionString;
    }

    // Méthode pour récupérer une personne par son ID
    public Personne? ObtenirPersonneParId(int personneId)
    {
        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Personne WHERE PersonneId = @PersonneId";
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@PersonneId", personneId, DbType.Int32));
                
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return MapPersonne(reader);
                    }
                }
            }
        }

        return null; // Aucune personne trouvée pour l'ID spécifié
    }

    // Méthode pour créer une nouvelle personne
    public void AjouterPersonne(Personne personne)
    {
        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO Personne (Nom, Prenom, DateNaissance) VALUES (@Nom, @Prenom, @DateNaissance); SELECT last_insert_rowid();";
                
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Nom", personne.Nom, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Prenom", personne.Prenom, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@DateNaissance", personne.DateNaissance, DbType.DateTime));

                personne.PersonneId = Convert.ToInt32(command.ExecuteScalar());
            }
        }
    }

    // Méthode pour récupérer toutes les personnes
    public List<Personne> ObtenirToutesLesPersonnes()
    {
        List<Personne> personnes = new List<Personne>();

        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Personne";
                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Personne personne = MapPersonne(reader);
                        personnes.Add(personne);
                    }
                }
            }
        }

        return personnes;
    }

    // Méthode pour récupérer toutes les adresses d'une personne
    public List<Adresse> ObtenirAdressesPersonne(int personneId)
    {
        List<Adresse> adresses = new List<Adresse>();

        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM Adresse WHERE PersonneId = @PersonneId";
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@PersonneId", personneId, DbType.Int32));

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Adresse adresse = AdresseRepository.MapAdresse(reader);
                        adresses.Add(adresse);
                    }
                }
            }
        }

        return adresses;
    }

    // Méthode pour mettre à jour une personne
    public void MettreAJourPersonne(Personne personne)
    {
        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "UPDATE Personne SET Nom = @Nom, Prenom = @Prenom, DateNaissance = @DateNaissance WHERE PersonneId = @PersonneId";
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Nom", personne.Nom, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@Prenom", personne.Prenom, DbType.String));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@DateNaissance", personne.DateNaissance, DbType.DateTime));
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@PersonneId", personne.PersonneId, DbType.Int32));

                command.ExecuteNonQuery();
            }
        }
    }

    // Méthode pour supprimer une personne
    public void SupprimerPersonne(int personneId)
    {
        using (IDbConnection connection = factory.CreateConnection())
        {
            connection.ConnectionString = connectionString;
            connection.Open();

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "PRAGMA foreign_keys = ON; DELETE FROM Personne WHERE PersonneId = @PersonneId;";
                command.Parameters.Add(Utils.CreateParameter(command.CreateParameter(), "@PersonneId", personneId, DbType.Int32));

                command.ExecuteNonQuery();
            }
        }
    }

    // Méthode utilitaire pour mapper les données du lecteur à l'objet Personne
    public static Personne MapPersonne(IDataReader reader)
    {
        return new Personne
        {
            PersonneId = Convert.ToInt32(reader["PersonneId"]),
            Nom = (string) reader["Nom"],
            Prenom = (string) reader["Prenom"],
            DateNaissance = (DateTime) reader["DateNaissance"]
        };
    }
}
