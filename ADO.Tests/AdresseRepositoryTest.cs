using System.Data.Common;
using System.Data.SQLite;

namespace ADO.Tests;

[TestFixture]
public class AdresseRepositoryTest
{
    private AdresseRepository adresseRepository;
    private string ConnectionString;

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {

    }

    [SetUp]
    public void Setup()
    {
        string query = @"
        DROP TABLE IF EXISTS Adresse;
        DROP TABLE IF EXISTS Personne;

        CREATE TABLE Personne (
            PersonneId INTEGER PRIMARY KEY AUTOINCREMENT,
            Nom TEXT NOT NULL,
            Prenom TEXT NOT NULL,
            DateNaissance DATE NOT NULL
        );

        CREATE TABLE Adresse (
            AdresseId INTEGER PRIMARY KEY AUTOINCREMENT,
            PersonneId INTEGER NOT NULL,
            Rue TEXT NOT NULL,
            Ville TEXT NOT NULL,
            CodePostal TEXT NOT NULL,
            FOREIGN KEY (PersonneId) REFERENCES Personne(PersonneId) ON DELETE CASCADE ON UPDATE CASCADE
        );";
    }

    [Test]
    public void TestObtenirAdresseParId()
    {
        
    }
}